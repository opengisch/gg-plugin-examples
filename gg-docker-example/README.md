# Run GeoGirafe in a container


The idea behind the follwowing sections is to showcase how you could run GeoGirafe in a container.

This is especially usefull if you want to avaoid digging too deep into the internals of GeoGirafe
and want to concentrate more on the cartographic content your webgis offers. However, small
customizations are possible with the described approaches too:

- [Run and enjoy](base.run/README.md)
- [Modify Icon and run](custom.icon.run/README.md)
- [Modify App and run](custom.app.run/README.md)
- [Build your own](custom.app.build/README.md)
- [Build for favourit webserver](custom.webserver.build/README.md)

The replacement approaches are really handy. If you want to go more adventurous you can look at other files and replace them. This can go with everything. Starting from other logos e.g. `favicon` up to the css in the assets folder and finally the index.html itself to rearrange or manipulate the look of your GeoGirafe instance.

While this approach is simple and lean you should always recheck when it is time to move over to a more DEVish
approach. But for small changes this will do also production environment requirements.
