# Build for favourit webserver

You might be in the need of a custom webserver (internal restrictions). This small showcase demontrates how
you can easily handle such requirements. In this example we use NGINX.

For that we write our own [Dockerfile](Dockerfile) and add the config there.

The final run command then is shorter and we have clearly defined deliverables (the built docker images).

```bash
docker build -t geogirafe-viewer:custom.webserver .
```

After build was finished you can run your personal instance of GeoGirafe by:

```bash
docker run \
  -p 8080:80 \
  geogirafe-viewer:custom.webserver
```

This makes use of the following:

- custom [config.json](config/config.sitn.json)
- dynamically loaded themes json
- dynamically loaded translations
- the app is served by [NGINX](https://hub.docker.com/_/nginx)
