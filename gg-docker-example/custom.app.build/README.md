# Build your own

Instead of purely running a original instance of GeoGirafe and overwrite assets on runtime you can
produce (build) your custom images to have a more package like experience (versioning, tests and different stages).

For that we write our own [Dockerfile](Dockerfile) and add the assets there.

The final run command then is shorter and we have clearly defined deliverables (the built docker images).

```bash
docker build -t geogirafe-viewer:custom.app .
```

After build was finished you can run your personal instance of GeoGirafe by:

```bash
docker run \
  -p 8080:80 \
  geogirafe-viewer:custom.app
```

This makes use of the following:

- custom [config.json](config/config.sitn.json)
- custom [logo_small](assets/logo_mapbs.png)
- custom [logo_black_small](assets/logo_mapbs_dark.png)
- custom [index.html](index.html)
- dynamically loaded themes json
- dynamically loaded translations

There are plenty of possibilities to customize things. Just try it out!
