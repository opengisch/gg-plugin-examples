# Modify Icon and run

You may want to customize small things in the GeoGirafe application. The most prominent item for this is
clearly the logo. This is really easy. We can to so with mounting alternate versions of the logo into the
container. This is really lean and still saves us from too much DEV interaction with the GeoGireafe code. We only need to know
what structure the served static folder has inside the container.

To help out here is a part of it (some parts are skipped for the sake of simplicity!):

```
/var/www/localhost/htdocs/
├── about.json
├── android-chrome-192x192.png
├── android-chrome-512x512.png
├── apple-touch-icon.png
├── assets
│   ├── Cesium-216d2399.js
│   ├── Cesium-216d2399.js.map
│   ├── OLCesium-b40bb9cf.js
│   ├── OLCesium-b40bb9cf.js.map
│   ├── __vite-browser-external-024afdea.js
│   ├── __vite-browser-external-024afdea.js.map
│   ├── index-2876aa95.js
│   ├── index-2876aa95.js.map
│   ├── index-56e67bfe.css
│   ├── lazy-af36d162.js
│   └── lazy-af36d162.js.map
├── components
│   ├── about
│   │   └── images
│   │       └── logo.png
│   └── help
│       └── images
│           ├── arrow_black.png
│           ├── arrow_white.png
│           └── arrow_white.webp
├── config.json
├── favicon-16x16.png
├── favicon-32x32.png
├── favicon.ico
├── images
│   ├── girafe.png
│   ├── logo.png
│   ├── logo_black.png
│   ├── logo_black_small.png
│   ├── logo_black_small.webp
│   ├── logo_small.png
│   └── logo_small.webp
├── index.html
├── lib
│   ├── cesium
│   │   └──...[content skipped]
│   ├── font-gis
│   │   └── ...[content skipped]
│   ├── fontawesome
│   │   ├── ...[content skipped]
│   ├── gridjs
│   │   └── ...[content skipped]
│   ├── openlayers
│   │   └── ...[content skipped]
│   ├── tippy.js
│   │   └── ...[content skipped]
│   └── vanilla-picker
│       └── ...[content skipped]
└── ...[content skipped]
```

As you can see there are a lot of static files. the ones we are interested in are located in the `images` folder. We see that tere is a `logo_` and a `logo_black`. These are used because GeoGirafe has a dark mode. So we need to overwrite these two logos and we should see it in our instance directly. It took a bit of try and error to find which logo we need to overwrite. But on a normal screen it works with the following command:

```bash
docker run \
  -v $(pwd)/config/config.sitn.json:/var/www/localhost/htdocs/config.json \
  -v $(pwd)/assets/logo_mapbs.png:/var/www/localhost/htdocs/images/logo_small.webp \
  -v $(pwd)/assets/logo_mapbs_dark.png:/var/www/localhost/htdocs/images/logo_black_small.webp \
  -p 8080:80 \
  geogirafe/viewer
```

This makes use of the following:

- custom [config.json](config/config.sitn.json)
- custom [logo_small](assets/logo_mapbs.png)
- custom [logo_black_small](assets/logo_mapbs_dark.png)
- dynamically loaded themes json
- dynamically loaded translations

Of course you need to respect the original dimensions of the logos to not blow the visual style of GeoGirafe.
