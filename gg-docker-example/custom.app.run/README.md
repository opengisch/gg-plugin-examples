# Modify App and run

Beside replacing assets to bend GeoGirafe to be your personal webgis you can also modify the apps appearience
itself. Lets assume you want to make a clean and easy webgis app for your customers and remove the more expert
functions. You can achieve this by simply mount a custom index.html into the container. The original file is located
here:

```
/var/www/localhost/htdocs/index.html
```
In this example we replace the original HTML file with our [custom version](index.html). Thanks to the modular approach of GeoGirafe we removed several elements from the top
toolbar and the scale chooser from the bottom to provide a simple webgis.

To fire up our custom instance you need to issue the following command:

```bash
docker run \
  -v $(pwd)/config/config.sitn.json:/var/www/localhost/htdocs/config.json \
  -v $(pwd)/index.html:/var/www/localhost/htdocs/index.html \
  -p 8080:80 \
  geogirafe/viewer
```

This makes use of the following:

- custom [config.json](config/config.sitn.json)
- custom [index.html](index.html)
- dynamically loaded themes json
- dynamically loaded translations

Keep in mind that this my have side effects over time. The original `index.html` will change and you need to update your custom
one to stay in sync. Still, this is a handy approach to easily customize your personal GeoGirafe instance without any further
tools.
