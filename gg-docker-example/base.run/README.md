# Run and enjoy

The most simplistic approach to enjoy the features of gegirafe is this:

```bash
docker run -v $(pwd)/config/config.sitn.json:/var/www/localhost/htdocs/config.json -p 8080:80 geogirafe/viewer
```

This makes use of the following:

- a [config.json](config/config.sitn.json) => read more about how to configure GeoGirafe [here](https://doc.geomapfish.dev/docs/about)
- dynamically loaded themes json
- dynamically loaded translations


After short time a service is running and you can access your instance with a [browser](http://localhost:8080).
