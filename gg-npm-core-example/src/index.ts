import gg from '@opengisch/geogirafe-core';

// Extend default Document and Window interfaces
declare global {
    interface Document {
      state: any;
    }
    interface Window {
      CESIUM_BASE_URL: string;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      Cesium: any;
    }
  }

// Add the state to document, so that it will be accessible everywhere
document.state = gg.core.StateManager.getInstance().state;

customElements.define('girafe-button', gg.components.ButtonComponent);

gg.core.elements.GirafeHTMLElement 
// subclassing the default component to add a custom method
// add map
